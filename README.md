# GTK 2 Mod
This is a patch for GTK 2 that changes the modification date and time format to be more in-line with GTK 3's. It is based on the upstream package provided by Arch Linux (see **PKGBUILD**), and so also contains an additional patch provided by Arch Linux.

## Installation
No changes were made to the compilation procedure, other than using **quilt** to patch the sources (see **PKGBUILD**). For the manual installation instructions, see the *build()* function in **PKGBUILD**.
