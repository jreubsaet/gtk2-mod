# Maintainer: J. Reubsaet <jesusofsuburbia@tuta.io>
# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Maintainer: Jan de Groot <jgc@archlinux.org>

pkgname=gtk2-mod
pkgver=2.24.33
pkgrel=1
pkgdesc="GTK2 with a modified date-time format and default settings"
arch=(x86_64)
url="https://www.gtk.org/"
depends=(atk pango libxcursor libxinerama libxrandr libxi libxcomposite libxdamage
	 shared-mime-info cairo libcups gtk-update-icon-cache librsvg desktop-file-utils)
makedepends=(gobject-introspection git gtk-doc quilt)
optdepends=('xfce-theme-greybird: Default widget theme'
	    'elementary-xfce-icons: Default icon theme'
	    'python2: gtk-builder-convert')
provides=(gtk2=2.24.33 libgailutil.so libg{d,t}k-x11-2.0.so)
conflicts=(gtk2)
license=(LGPL)
install=gtk2.install
_commit=68631945733158f164427db84f01301d7e875763  # tags/2.24.33^0
source=("git+https://gitlab.gnome.org/GNOME/gtk.git#commit=$_commit"
	file-chooser__datetimeformat.patch
	gtkrc
	gtk-query-immodules-2.0.hook
	xid-collision-debug.patch
	series)
sha256sums=('SKIP'
            'f06d8529ff2703c1d3ef500dc716933c944f2b7de29a0727a66a563c09fad229'
            '1a40a018ca831560eb4df822f40cf124069908f93af063b142be71be777ad0e0'
            '427e7ed2626465ea7a8986189b6c8340a0520b9f7b8e2a56cd1169d375da7425'
            '7724a55c4916318ed81b22f21c37c0a66e78adce017563fed6d84d813a4b80bc'
            '8d2e1eed8e5f3e050f1e6c320c44ac8bea898dffd8e608b459321b8289f1fe5c')

pkgver() {
	cd gtk
	git describe --tags | sed 's/-/+/g'
}

prepare() {
	cd gtk
	QUILT_PATCHES=.. quilt push -av
	sed -i '1s/python$/&2/' gtk/gtk-builder-convert
	NOCONFIGURE=1 ./autogen.sh
}

build() {
	cd gtk
	./configure \
	--prefix=/usr \
	--sysconfdir=/etc \
	--localstatedir=/var \
	--with-xinput=yes \
	--disable-gtk-doc
# https://bugzilla.gnome.org/show_bug.cgi?id=655517
	sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
	make
}

package() {
	cd gtk
	make DESTDIR="$pkgdir" install

	install -Dt "$pkgdir/usr/share/gtk-2.0" -m644 ../gtkrc
	install -Dt "$pkgdir/usr/share/libalpm/hooks" -m644 ../gtk-query-immodules-2.0.hook

	rm "$pkgdir/usr/bin/gtk-update-icon-cache"
}
